<?php
/*
Plugin Name: Essential Grid Stickys
Plugin URI: https://www.colombiavip.com/
Description: Selecciona uno o más Grids con los IDS separados por comas "," y muestra los Stickys primero de dichos Grids.
Version: 1.0
Author: CVIP
Author URI: https://www.colombiavip.com/
*/

defined ('ABSPATH') or die ("No tienes permiso para estar aqui");

function eg_modify_post ($posts, $grid_id){
    $ids = get_option('essential_grid_stickys_option_ids');
    $arrayIds = explode(',', str_replace(' ', '', $ids));
    $onlyStickys = get_option('essential_grid_stickys_option_only_stickys');
    foreach ($arrayIds as $your_grid_id) {
      $stickys = array();
      $regular = array();
      foreach($posts as $post) {
          if(!is_sticky($post['ID'])) $regular[] = $post;
          else $stickys[] = $post;
      }
    }
    if ($onlyStickys){
      return $stickys;
    }else {
      return array_merge($stickys, $regular);
    }
}

# 1
function essential_grid_stickys_register_settings() {
   add_option( 'essential_grid_stickys_option_id', '1');
   add_option( 'essential_grid_stickys_option_ids', '1, 2');
   add_option( 'essential_grid_stickys_option_only_stickys', 'true');

   register_setting( 'essential_grid_stickys_options_group', 'essential_grid_stickys_option_id', 'essential_grid_stickys_callback' );
   register_setting( 'essential_grid_stickys_options_group', 'essential_grid_stickys_option_ids', 'essential_grid_stickys_callback' );
   register_setting( 'essential_grid_stickys_options_group', 'essential_grid_stickys_option_only_stickys', 'essential_grid_stickys_callback' );
}
add_action( 'admin_init', 'essential_grid_stickys_register_settings' );

#2
function essential_grid_stickys_register_options_page() {
  add_options_page('Essential Grid Stickys', 'Essential Grid Stickys', 'manage_options', 'essential-grid-stickys', 'essential_grid_stickys_options_page');
}
add_action('admin_menu', 'essential_grid_stickys_register_options_page');


# 3
function essential_grid_stickys_options_page() { ?>
  <div class="contenedor-essential-grid-stickys-options">
  <?php screen_icon(); ?>
  <h1>Essential Grid Stickys</h1>
  
	  <form method="post" action="options.php">
		  <?php settings_fields( 'essential_grid_stickys_options_group' ); ?>
		  <h3>Options</h3>
		  <table>
			<tr valign="center">
			  <th scope="row"><label for="essential_grid_stickys_option_ids">ID of Essential Grid</label></th>
			  <td><input type="text" id="essential_grid_stickys_option_ids" name="essential_grid_stickys_option_ids" value="<?php echo get_option('essential_grid_stickys_option_ids'); ?>" /></td>
			</tr>

			<tr valign="center">
			  <th scope="row"><label for="essential_grid_stickys_option_only_stickys">Only Stickys</label></th>
			  <td><input type="checkbox" id="essential_grid_stickys_option_only_stickys" name="essential_grid_stickys_option_only_stickys" value="essential_grid_stickys_option_only_stickys" <?php if (get_option('essential_grid_stickys_option_only_stickys') ){echo 'checked';} ?>></td>
			</tr>
		  </table>
		  <?php  submit_button(); ?>
	  </form>
	  
  </div>

  <style media="screen">
    .contenedor-essential-grid-stickys-options h1 {
      text-align: center;
    }

    .contenedor-essential-grid-stickys-options form th {
      text-align: left;
    }
  </style>
<?php
}

add_filter('essgrid_modify_posts',  'eg_modify_post', 10, 2);
