<?php
/*
Plugin Name:  Whatsapp Message Generator
Plugin URI:   https://colombiavip.com
Description:  Se usa con el Shortcode [cvip_wa titulo='Cvip Whtasapp Link Generator' code=57]
Version:      1.20201127
Author:       Colombia VIP
Author URI:   https://colombiavip.com
*/

class WhatsappMessageGenerator {

  function __construct(){
    add_action('init', array($this, 'init'));
    add_shortcode('cvip_wa', array($this, 'cvip_wa'));
  }

  public function init(){
    add_action('wp_enqueue_scripts', array($this, 'ajax_enqueue_scripts'));
    // Hook para usuarios no logueados
    add_action('wp_ajax_nopriv_action_cvip_wa', array($this, 'action_cvip_wa'));
    // Hook para usuarios logueados
    add_action('wp_ajax_action_cvip_wa', array($this, 'action_cvip_wa'));
  }

  public function cvip_wa($atts){
    $default = [
      'titulo' => 'Cvip Whtasapp Link Generator',
      'code' => 57
    ];
    $atts = ($atts) ? array_merge($default, $atts) : $default;

    // TODO: CARGAR ESTILOS (CSS)
    $urlActual = $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $atts['code'] = str_replace('+', '', str_replace(' ', '', $atts['code']));

    require 'view.php';
    return;
  }

  # Función que procesa la llamada AJAX
  function action_cvip_wa(){
    $code = $_POST['code'];
    $numero = $_POST['number'];
    $mensaje = str_replace(' ', '%20', $_POST['message']); ?>
    <input class="form-control ng-pristine ng-valid ng-touched" readonly="" type="text" value="<?="https://api.whatsapp.com/send?phone={$code}{$numero}&text=$mensaje"; ?>">
    <?php
    die;
  }

  function ajax_enqueue_scripts() {
    wp_enqueue_style( 'cvip-wa-style', plugins_url( '/style.css', __FILE__ ), array(), time() );
    wp_enqueue_script( 'cvip-wa-ajax', plugins_url( '/ajax.js', __FILE__ ), array('jquery'), time(), true );
    wp_localize_script('cvip-wa-ajax', 'WP_AJAX_WA', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
  }

}

new WhatsappMessageGenerator;
