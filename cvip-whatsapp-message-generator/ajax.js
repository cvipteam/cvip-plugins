jQuery(document).ready( function(){
	jQuery('.cvip-wa-ajax-resultado .cvip-wa-ajax-resultado-contenedor').hide();

	jQuery('.cvip-wa-form input[type="submit"]').on('click', function(event){
    event.preventDefault();

		var mensaje = jQuery('.cvip-wa-form input.cvip-wa-mensaje').val();
		var code = jQuery('.cvip-wa-form .cvip-wa-form-select option').filter(':selected').val();
		var numero = jQuery('.cvip-wa-form input.cvip-wa-numero').val();
		var postData = new FormData();

		postData.append("action", "action_cvip_wa");
		postData.append("message", mensaje);
		postData.append("code", code);
		postData.append("number", numero);

		// La llamada AJAX
    jQuery.ajax({
        type : "post",
        url  : WP_AJAX_WA.ajaxurl, // 'your-admin-ajax.php', // Pon aquí tu URL
        data : postData,
				processData: false,
				contentType: false,
				beforeSend: function() {
					 jQuery('.cvip-wa-ajax-resultado .cvip-wa-ajax-loader').html("<marquee class='marq' bgcolor=silver width=200 height=20 direction=left behavior=alternate>CARGANDO...</marquee>");
				},
				error: function(jqXHR, textStatus, errorMessage) {
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorMessage);
				},
        success: function(response) {
          // Actualiza el mensaje con la respuesta
					jQuery('.cvip-wa-ajax-resultado .cvip-wa-ajax-loader').html(' ');
					jQuery('.cvip-wa-ajax-resultado .cvip-wa-ajax-resultado-contenedor').show();
					jQuery('.cvip-wa-ajax-resultado .cvip-wa-ajax-link').html(response);
        }
    });
  });

	jQuery('.cvip-wa-ajax-resultado .cvip-wa-ajax-resultado-contenedor .cvip-wa-ajax-copy-link').on('click', function(event){
		var urlWa = jQuery('.cvip-wa-ajax-resultado .cvip-wa-ajax-link input').val();
		// creamos un input que nos ayudara a guardar el texto temporalmente
		var $temp = jQuery("<input>");
		// lo agregamos a nuestro body
		jQuery("body").append($temp);
		// Agregamos en el atributo value del input el contenido html encontrado
		// en el td que se dio click
		// y seleccionamos el input temporal
		$temp.val(urlWa).select();
		// Ejecutamos la funcion de copiado
		document.execCommand("copy");
		// Eliminamos el input temporal
		$temp.remove();

		var texto_copiar_link = jQuery(this).text();
		jQuery(this).text('Link Copiado');
		setTimeout(
	  function()
	  {
			jQuery('.cvip-wa-ajax-resultado .cvip-wa-ajax-resultado-contenedor .cvip-wa-ajax-copy-link').text(texto_copiar_link);
	  }, 3000);
	});

});
